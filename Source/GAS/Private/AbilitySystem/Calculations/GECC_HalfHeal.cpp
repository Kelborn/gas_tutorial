// Fill out your copyright notice in the Description page of Project Settings.


#include "GECC_HalfHeal.h"
#include "MinimalAttributeSet.h"

// Declare the attributes to capture and define how we want to capture them from the Source and Target.
struct UHalfHealStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Health)

	UHalfHealStatics()
	{
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMinimalAttributeSet, Health, Target, false);
	}
};

static const UHalfHealStatics& DamageStatics()
{
	static UHalfHealStatics DStatics;
	return DStatics;
}

UGECC_HalfHeal::UGECC_HalfHeal()
{
	RelevantAttributesToCapture.Add(DamageStatics().HealthDef);
}

void UGECC_HalfHeal::Execute_Implementation(const FGameplayEffectCustomExecutionParameters & ExecutionParams, OUT FGameplayEffectCustomExecutionOutput & OutExecutionOutput) const
{
	UAbilitySystemComponent* TargetAbilitySystemComponent = ExecutionParams.GetTargetAbilitySystemComponent();

	AActor* TargetActor = TargetAbilitySystemComponent ? TargetAbilitySystemComponent->AvatarActor : nullptr;

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	// Gather the tags from the source and target as that can affect which buffs should be used
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	float HealthValue = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().HealthDef, EvaluationParameters, HealthValue);
	HealthValue *= .5f;

	// Add modifier that equals to half of current health
	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(DamageStatics().HealthProperty, EGameplayModOp::Additive, HealthValue));
}