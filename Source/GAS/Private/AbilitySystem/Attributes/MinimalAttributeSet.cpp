// Fill out your copyright notice in the Description page of Project Settings.


#include "MinimalAttributeSet.h"
#include "GameplayEffectExtension.h"

void UMinimalAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData & Data)
{
	if (GetHealthAttribute() == Data.EvaluatedData.Attribute)
	{
		OnHealthChanged.Broadcast(Health.GetCurrentValue());
	}
}
