
#include "MyPawn.h"
#include "AbilitySystemComponent.h"
#include "Components/CapsuleComponent.h"
#include "MinimalAttributeSet.h"

#pragma region ...
// Sets default values
AMyPawn::AMyPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	RootComponent = Capsule;

	ASC = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("ASC"));
}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

UAbilitySystemComponent* AMyPawn::GetAbilitySystemComponent() const
{
	return ASC;
}

void AMyPawn::PossessedBy(AController * NewController)
{
	Super::PossessedBy(NewController);

	if (ASC)
	{
		ASC->InitAbilityActorInfo(this, this);
	}
}

UAttributeSet * AMyPawn::InitSet(TSubclassOf<UAttributeSet> SetClass, UDataTable * InitTable)
{
	if (ASC)
	{
		return const_cast<UAttributeSet*>(ASC->InitStats(SetClass, InitTable));
	}
	return nullptr;
}

void AMyPawn::GiveAbility(TSubclassOf<class UGameplayAbility> AbilityClass)
{
	if (ASC)
	{
		ASC->GiveAbility(FGameplayAbilitySpec(AbilityClass));
	}
}

#pragma endregion
