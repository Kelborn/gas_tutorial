#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "AbilitySystemInterface.h"
#include "MyPawn.generated.h"

UCLASS()
class GAS_API AMyPawn : public APawn, public IAbilitySystemInterface
{
	GENERATED_BODY()
#pragma region ...
public:
	// Sets default values for this pawn's properties
	AMyPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UCapsuleComponent* Capsule;


public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UAbilitySystemComponent* ASC;
	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;	
	
	void PossessedBy(AController* NewController) override;

	UFUNCTION(BlueprintCallable)
		class UAttributeSet* InitSet(TSubclassOf<class UAttributeSet> SetClass, class UDataTable* InitTable);

#pragma endregion
	UFUNCTION(BlueprintCallable)
		void GiveAbility(TSubclassOf<class UGameplayAbility> AbilityClass);
};