// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "GECC_HalfHeal.generated.h"

/**
 * 
 */
UCLASS()
class GAS_API UGECC_HalfHeal : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
public:
	UGECC_HalfHeal();

	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

};
