// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "MinimalAttributeSet.generated.h"

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAttributeChangedDelegate, float, CurrentValue);

UCLASS()
class GAS_API UMinimalAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
	
public:
	UPROPERTY(Category = "Attributes", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UMinimalAttributeSet, Health)

	UPROPERTY(Category = "Attributes | Events", VisibleAnywhere, BlueprintAssignable)
	FOnAttributeChangedDelegate OnHealthChanged;

	UPROPERTY(Category = "Attributes", EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData Ammo;
	ATTRIBUTE_ACCESSORS(UMinimalAttributeSet, Ammo)

	virtual void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData &Data) override;
};